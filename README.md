[![star](https://gitee.com/itsay/resume/badge/star.svg?theme=white)](https://gitee.com/itsay/resume/stargazers)    [![fork](https://gitee.com/itsay/resume/badge/fork.svg?theme=white)](https://gitee.com/itsay/resume/members)

# [李煜华个人简历](http://nfunm041.gitee.io/resume)
<img src="assets/images/avatar2.png" width="150px" height="150px" align="center">

### 个人评价
喜欢每天坚持跑步3公里并且附加其他运动和健身达2个小时的习惯。除此之外我还喜欢了解一些前沿技术并且适当的赋予实际的操作。目前个人正在努力掌握专业知识外，也在扩展自身的专业衍生技能部分的内容，我认为复合型人才就是要主动出击才行。其实我也喜欢玩，不过有时候，学东西就是玩的一部分，我就是那种喜欢把各种不同类别的东西玩在一起的人，无论是游戏还是音乐，又或者是专业技术，还是文学艺术。

### pc端
![](assets/images/pc.png)

### 移动端
![](assets/images/ip.png)


